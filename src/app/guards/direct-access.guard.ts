import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';

@Injectable({
  providedIn: 'root'
})
export class DirectAccessGuard implements CanActivate {
  constructor(private router: Router, private session: SessionService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // If the previous URL was blank, then the user is directly accessing this page

    var sess = this.session.getSession();
    if (!sess) {
      this.router.navigate(['/']); // Navigate away to some other page
      return false;
    }
    return true;
  }

}
