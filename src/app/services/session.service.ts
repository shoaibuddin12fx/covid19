import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  getSession(){
    return sessionStorage.getItem('api_key');
  }

  setSession(api_key){
    sessionStorage.setItem('api_key', api_key );
  }

  setKey(key, value){
    localStorage.setItem(key, value);
  }

  getKey(key){
    localStorage.getItem(key);
  }
}
