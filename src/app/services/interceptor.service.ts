import { SessionService } from './session.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.callToken()).pipe( switchMap( value => {
      let cloneRequest = this.addSecret(req, value);
      return next.handle(cloneRequest)
    }));


  }

  callToken(){
    return new Promise( resolve => {
      let api_key = sessionStorage.getItem('api_key');
      resolve(api_key);
    })

  //return this.storage.get('token');
}

  // will be used later for google login
  private addSecret(request: HttpRequest<any>, value) {

    var obj = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    }
    if(value){
      obj['Authorization'] = value
    }

    const clone = request.clone({
      setHeaders: obj,
      withCredentials: false
    });
    return clone;
  }
}
