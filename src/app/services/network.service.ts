import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service'
import { config } from '../shared/config/api.config';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  urls: any = config.api;
  constructor(public http: HttpClient,
              public api: ApiService) {

  }

  getDownloadReports(){
    return this.httpGetResponse('api/downloadRecords');
  }

  postCheckLogin(data){
    return this.httpPostResponse('api/checklogin', data)
  }

  postUserLogin(data){
    return this.httpPostResponse('api/login', data)
  }

  postUserReport(data) {
    return this.httpPostResponse('api/postCovidReport', data)
  }

  // Generic Methods for Http Response
  /**
   * @param key
   * @param data
   * @param id
   * @param showloader
   * @param showError
   */

  httpPostResponse(key, data, id = null, showloader = false, showError = false) {
    return this.httpResponse('post', key, data, id, showloader, showError);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true) {
    return this.httpResponse('get', key, {}, id, showloader, showError);
  }

  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true) {

    return new Promise(resolve => {

      const _id = (id) ? '/' + id : '';
      const url = key + _id;
      const seq = (type == 'get') ? this.api.get(url) : this.api.post(url, data);

      seq.subscribe((res: any) => {
        if (res['bool'] != true) {
          resolve(null);
        } else {
          resolve(res);
        }

      }, err => {

        resolve(null);

      });

    });

  }

}
