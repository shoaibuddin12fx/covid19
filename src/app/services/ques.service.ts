import { Injectable } from '@angular/core';

declare var generateQuestionnaire: any;

@Injectable({
  providedIn: 'root'
})
export class QuesService {

  e: any;
  stopData: any;
  evalues: any;

  private f;
  constructor() {
    this.f = new generateQuestionnaire();
    this.e = {};
    this.stopData = {};
    this.evalues = {};
  }

  getEvalue(){
    return this.evalues;
  }

  setEvalue(obj){
    this.evalues[obj["phase"]] = obj["answers"];
  }

  resetEvalue(){
    this.evalues = {}
  }

  resetPhase(){
    this.e = {};
  }

  getPhase(t){
    return new this.f.function(t);
  }

  getUserPhaseOptions(){
    return this.f.user_phase();
  }

  setUserPhaseOptions(v){
    this.e['user_single_choice'] = v;
  }

  getEmergencyPhaseOptions(v){
    return this.f.emergency_phase(v);
  }

  setEmergencyPhaseOptions(v){
    this.e['emergency_single_choice'] = v;
  }

  getAgePhaseOptions(){
    return this.f.age_phase(this.e['user_single_choice']);
  }

  setAgePhaseOptions(v){
    this.e['age_single_choice'] = v;
  }

  getSymptomPhaseOptions(){
    return this.f.symptom_phase(this.e['user_single_choice']);
  }

  setSymptomPhaseOptions(v){
    this.e['symptom_multiple_choice'] = v;
  }

  getConditionPhaseOptions(){
    return this.f.condition_phase(this.e['user_single_choice']);
  }

  setConditionPhaseOptions(v){
    this.e['condition_multiple_choice'] = v;
  }

  getTravelPhaseOptions(){
    return this.f.travel_phase(this.e['user_single_choice']);
  }

  setTravelPhaseOptions(v){
    this.e['travel_single_choice'] = v;
  }

  getExposurePhaseOptions(){
    return this.f.exposure_place_phase(this.e['user_single_choice']);
  }

  setExposurePhaseOptions(v){
    this.e['exposure_place_multiple_choice'] = v;
  }

  getExposurePersonPhaseOptions(){
    return this.f.exposure_person_phase(this.e['user_single_choice']);
  }

  setExposurePersonPhaseOptions(v){
    this.e['exposure_person_multiple_choice'] = v;
  }

  getCarePhaseOptions(){
    return this.f.care_facility_phase(this.e['user_single_choice']);
  }

  setCarePhaseOptions(v){
    this.e['care_facility_single_choice'] = v;
  }

  evaluate(){
    return this.f.evaluator(this.e);
  }

  getStopData(){
    return this.stopData
  }

  setStopData(data){
    this.stopData = data;
  }





}

