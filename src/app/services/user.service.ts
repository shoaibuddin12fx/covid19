import { NetworkService } from './network.service';
import { SessionService } from './session.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: any
  constructor(private session: SessionService, private network: NetworkService) { }

  setUser(user){
    this.user = user;
  }

  getUser(){
    return this.user;
  }

  initialized(){

    let key = this.session.getSession();
    if(!key){

      let email = 'user' + Date.now() + '@mail.com';
      this.network.postUserLogin({email: email}).then( v => {
        if(v){
          this.user = v['user'];
          this.session.setSession(this.user.api_key);
          this.session.setKey('email', this.user.email);
        }
      });


    }


  }






}
