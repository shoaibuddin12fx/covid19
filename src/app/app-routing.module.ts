import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CovidComponent } from './pages/covid/covid.component';
import { HomePageRoutingModule } from './pages/home/home-routing.module';
import { ErrorComponent } from './pages/error/error.component';
import { DirectAccessGuard } from './guards/direct-access.guard';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HomePageRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
