import { UserService } from './services/user.service';
import { Router } from '@angular/router';
import { NetworkService } from './services/network.service';
import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'تشخيص أعراض فيروس كورونا';

  constructor(private translate: TranslateService, public router: Router, public network: NetworkService, public userService: UserService) {
    translate.setDefaultLang('ar');

    // if no session // no user
    // create one and store in session
    this.userService.initialized();


  }

  download(){
    this.router.navigate(['download'])
  }

}
