import { Injector } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

export abstract class BasePage {

    public activatedRoute: ActivatedRoute;
    public router: Router;
    public location: Location;

    constructor(injector: Injector) {
        this.router = injector.get(Router);
        this.activatedRoute = injector.get(ActivatedRoute);
        this.location = injector.get(Location);
    }

    navigateTo(link, data?: NavigationExtras){
        this.router.navigate([link], data);
    }

    navigateToChild(link, data?: NavigationExtras){
        data.relativeTo = this.activatedRoute;
        this.router.navigate([link], data);
    }

    getParams() {
        return this.activatedRoute.snapshot.params;
    }

    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }
}
