import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HomePageRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { TranslateModule } from '@ngx-translate/core';
import { InstructionsComponent } from './instructions/instructions.component';
import { StartComponent } from './start/start.component';
import { StopComponent } from './stop/stop.component';
import { DownloadComponent } from './download/download.component';



@NgModule({
  entryComponents: [

  ],
  declarations: [
    HomeComponent,
    WelcomeComponent,
    InstructionsComponent,
    StartComponent,
    StopComponent,
    DownloadComponent
  ],
  exports: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HomePageRoutingModule,
    TranslateModule
  ],
  providers: [
  ]
})
export class HomePageComponentModule { }
