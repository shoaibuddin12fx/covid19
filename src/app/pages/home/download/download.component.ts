import { SessionService } from './../../../services/session.service';
import { NetworkService } from './../../../services/network.service';
import { UtilityService } from './../../../services/utility.service';
import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { QuesService } from 'src/app/services/ques.service';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent extends BasePage implements OnInit {

  email: any = 'mail';
  constructor(injector: Injector, private utility: UtilityService,  public ques: QuesService, public session: SessionService, private network: NetworkService) {
    super(injector);

    this.email = this.session.getKey('email');
  }

  ngOnInit(): void {

  }

  download(){

    if (this.utility.validateEmail(this.email)) {

      this.network.postCheckLogin({email: this.email}).then( v => {
        if(v){
          let user = v['user'];
          this.ques.resetEvalue();
          this.session.setSession(user.api_key);
          this.session.setKey('email', user.email);


          this.network.getDownloadReports().then( v => {

          })


        }else{
          alert("Invalid Email");
        }
      });
    }

  }

}
