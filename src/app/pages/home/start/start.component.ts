import { Component, OnInit, Injector } from '@angular/core';
import { Location } from '@angular/common';
import { BasePage } from '../base-page/base-page';
import { QuesService } from './../../../services/ques.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent extends BasePage implements OnInit {

  pre_value = 0;
  level = "emergency_phase";
  viewObj: any;
  selectedArray = [];

  constructor(injector: Injector, public ques: QuesService) {
    super(injector);
    this.initialize();
  }

  ngOnInit(): void {

  }

  async initialize() {
    this.pre_value = await this.ques.e.user_single_choice;
    this.getPhaseOptions(this.level);
  }

  getPhaseOptions(level) {
    switch (level) {
      case "emergency_phase":
        this.selectedArray = [];
        let f = this.ques.getEmergencyPhaseOptions(this.pre_value);
        this.viewObj = Object.assign({}, f);
        break;
      case "age_phase":
        this.selectedArray = [];
        let g = this.ques.getAgePhaseOptions();
        this.viewObj = Object.assign({}, g);
        break;

      case "symptom_phase":
        this.selectedArray = [];
        let h = this.ques.getSymptomPhaseOptions();
        this.viewObj = Object.assign({}, h);
      break;
      case "condition_phase":
        this.selectedArray = [];
        let i = this.ques.getConditionPhaseOptions();
        this.viewObj = Object.assign({}, i);
      break;
      case "travel_phase":
        this.selectedArray = [];
        let j = this.ques.getTravelPhaseOptions();
        this.viewObj = Object.assign({}, j);
      break;
      case "exposure_place_phase":
        this.selectedArray = [];
        let k = this.ques.getExposurePhaseOptions();
        this.viewObj = Object.assign({}, k);
      break;
      case "exposure_person_phase":
        this.selectedArray = [];
        let l = this.ques.getExposurePersonPhaseOptions();
        this.viewObj = Object.assign({}, l);
      break;
      case "care_facility_phase":
        this.selectedArray = [];
        let m = this.ques.getCarePhaseOptions();
        this.viewObj = Object.assign({}, m);
      break;







    }
  }

  setValue(value, length = 0) {
    switch (this.level) {
      case "emergency_phase":
        this.singleSelect(value)
        break;
      case "age_phase":
        this.singleSelect(value)
        break;
      case "symptom_phase":

        if(value == length - 1){
          this.selectedArray = [];
        }else{
          this.removeLastSelectionFromArray(length - 1)
        }
        this.multiSelect(value)
        break;
      case "condition_phase":

        if(value == length - 1){
          this.selectedArray = [];
        }else{
          this.removeLastSelectionFromArray(length - 1)
        }

        this.multiSelect(value)
        break;
      case "travel_phase":
        this.singleSelect(value)
        break;
      case "exposure_place_phase":

        if(value == length - 1 || value == length - 2){
          this.selectedArray = [];
        }else{
          this.removeLastSelectionFromArray(length - 1);
          this.removeLastSelectionFromArray(length - 2);
        }

        this.multiSelect(value);
      break;
      case "exposure_person_phase":
        this.singleSelect(value);
      break;
      case "care_facility_phase":
        this.singleSelect(value);
      break;

    }

  }

  singleSelect(value){
    this.pre_value = value;
    this.selectedArray = [];
    this.selectedArray.push(value);
  }

  multiSelect(value){
    let findIndex = this.selectedArray.indexOf(value);
    if(findIndex != -1){
      this.selectedArray.splice(findIndex, 1);
    }else{
      this.selectedArray.push(value);
    }
  }

  removeLastSelectionFromArray(value){
    let findIndex = this.selectedArray.indexOf(value);
    if(findIndex != -1){
      this.selectedArray.splice(findIndex, 1);
    }
  }

  next(res) {

    var self = this;
    function setStop(v){
      self.ques.setStopData(v.stop);
      self.ques.resetPhase();
      self.router.navigate(['/stop']);
    }

    this.setUserPhaseEvalue()

    switch (this.level) {
      case "emergency_phase":
        this.ques.setEmergencyPhaseOptions(this.pre_value);
        let v = this.ques.evaluate();
        if (v.stop) {
          setStop(v)
        } else {
          this.level = "age_phase";
          this.getPhaseOptions(this.level)
        }


        break;

      case "age_phase":

        this.ques.setAgePhaseOptions(this.pre_value);
        let w = this.ques.evaluate();
        if (w.stop) {
          setStop(w)
        } else {
          this.level = "symptom_phase";
          this.getPhaseOptions(this.level)

        }

        break;

      case "symptom_phase":

        this.ques.setSymptomPhaseOptions(this.selectedArray);
        let x = this.ques.evaluate();
        if (x.stop) {
          setStop(x)
        } else {
          this.level = "condition_phase";
          this.getPhaseOptions(this.level)

        }

      break;

      case "condition_phase":

        this.ques.setConditionPhaseOptions(this.selectedArray);
        let y = this.ques.evaluate();
        if (y.stop) {
          setStop(y)
        } else {
          this.level = "travel_phase";
          this.getPhaseOptions(this.level)

        }

      break;

      case "travel_phase":
        this.ques.setTravelPhaseOptions(this.selectedArray);
        let z = this.ques.evaluate();
        if (z.stop) {
          setStop(z)
        } else {
          this.level = "exposure_place_phase";
          this.getPhaseOptions(this.level);

        }
      break;
      case "exposure_place_phase":
        this.ques.setExposurePhaseOptions(this.selectedArray);
        let a = this.ques.evaluate();
        if (a.stop) {
          setStop(a)
        } else {
          this.level = "exposure_person_phase";
          this.getPhaseOptions(this.level);

        }
      break;
      case "exposure_person_phase":
        this.ques.setExposurePersonPhaseOptions(this.selectedArray);
        let b = this.ques.evaluate();
        if (b.stop) {
          setStop(b)
        } else {
          this.level = "care_facility_phase";
          this.getPhaseOptions(this.level);

        }
      break;
      case "care_facility_phase":
        this.ques.setCarePhaseOptions(this.selectedArray);
        let c = this.ques.evaluate();
        this.router.navigate(['/covid']);
      break;

    }


  }

  setUserPhaseEvalue(){
    let choices = this.viewObj.form.sections[0].answer.choices;
    let answers = choices.filter( v => {
       return this.selectedArray.includes( v.value )
      }).map( x => {
        return x.localizedLabel
      });

    let obj = {
      phase: this.level,
      answers: answers
    }

    this.ques.setEvalue(obj);
  }

  goBack() : void {

    switch (this.level) {
      case "care_facility_phase":
        this.level = "exposure_person_phase";
        this.getPhaseOptions(this.level)
        break;

      case "exposure_person_phase":
        this.level = "exposure_place_phase";
        this.getPhaseOptions(this.level)
        break;

      case "exposure_place_phase":
        this.level = "travel_phase";
        this.getPhaseOptions(this.level)
      break;

      case "travel_phase":
        this.level = "condition_phase";
        this.getPhaseOptions(this.level);
      break;

      case "condition_phase":
        this.level = "symptom_phase";
        this.getPhaseOptions(this.level);
      break;

      case "symptom_phase":
        this.level = "age_phase";
        this.getPhaseOptions(this.level);
      break;

      case "age_phase" :
        this.level = "emergency_phase";
        this.getPhaseOptions(this.level);
      break;

      case "emergency_phase":
        this.ques.resetPhase();
        this.router.navigate(['/instructions']);
      break;

    }
  }

  isSelected(v) {
    return this.selectedArray.includes(v)
  }

}
