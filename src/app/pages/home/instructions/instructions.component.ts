import { QuesService } from './../../../services/ques.service';
import { Component, OnInit, Injector } from '@angular/core';
import { Location } from '@angular/common';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.scss']
})
export class InstructionsComponent extends BasePage implements OnInit {

  choices: any[] = [];
  constructor(injector: Injector, public ques: QuesService) {
    super(injector)
  }



  ngOnInit(): void {

    let f = this.ques.getUserPhaseOptions();
    this.choices = f.form.sections[0].answer.choices;


  }

  startForm(value){
    this.ques.setUserPhaseOptions(value);
    this.setUserPhaseEvalue(value);
    this.router.navigate(['/start']);
  }

  setUserPhaseEvalue(value){

    let choice = this.choices.find( v => v.value == value);

    let obj = {
      phase: "user_phase",
      answers: [choice.localizedLabel]
    }

    this.ques.setEvalue(obj);
  }


}
