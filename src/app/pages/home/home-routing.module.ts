import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { HomeComponent } from './home.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { StartComponent } from './start/start.component';
import { StopComponent } from './stop/stop.component';
import { CovidComponent } from '../covid/covid.component';
import { DirectAccessGuard } from 'src/app/guards/direct-access.guard';
import { DownloadComponent } from './download/download.component';


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                redirectTo: 'welcome',
                pathMatch: 'full'
            },
            {
                path: 'welcome',
                component: WelcomeComponent,
            },
            {
              path: 'instructions',
              component: InstructionsComponent,
              canActivate: [DirectAccessGuard]
            },
            {
                path: 'start',
                component: StartComponent,
                canActivate: [DirectAccessGuard]
            },
            {
                path: 'stop',
                component: StopComponent,
                canActivate: [DirectAccessGuard]
            },

        ]
    },
    {
      path: 'covid',
      component: CovidComponent,
      canActivate: [DirectAccessGuard]
    },
    {
      path: 'download',
      component: DownloadComponent,
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomePageRoutingModule { }
