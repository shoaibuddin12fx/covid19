import { UserService } from './../../../services/user.service';
import { NetworkService } from './../../../services/network.service';
import { UtilityService } from './../../../services/utility.service';
import { SessionService } from './../../../services/session.service';
import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { QuesService } from 'src/app/services/ques.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent extends BasePage implements OnInit {

  email: any = '';

  constructor(injector: Injector,
              private ques: QuesService,
              private session: SessionService,
              private userService: UserService,
              public network: NetworkService,
              public utility: UtilityService ) {
    super(injector);

  }

  ngOnInit(): void {



  }

  startWelcome(){

    this.ques.resetEvalue();
    this.navigateTo('/instructions');

  }

}
