import { Component, OnInit, Injector } from '@angular/core';
import { QuesService } from './../../../services/ques.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-stop',
  templateUrl: './stop.component.html',
  styleUrls: ['./stop.component.scss']
})
export class StopComponent extends BasePage  implements OnInit {

  viewObj: any;
  constructor(injector: Injector, public ques: QuesService) {
    super(injector);
    this.initialize();
  }

  ngOnInit(): void {
  }

  async initialize(){
    this.viewObj = await this.ques.getStopData();
  }

}
