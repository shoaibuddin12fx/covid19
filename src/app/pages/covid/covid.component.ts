import { SessionService } from './../../services/session.service';
import { NetworkService } from './../../services/network.service';
import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../home/base-page/base-page';
import { QuesService } from 'src/app/services/ques.service';

@Component({
  selector: 'app-covid',
  templateUrl: './covid.component.html',
  styleUrls: ['./covid.component.scss']
})
export class CovidComponent extends BasePage implements OnInit {

  response: any;
  constructor(injector: Injector, public ques: QuesService, public network: NetworkService, public session: SessionService) {
    super(injector)
  }

  ngOnInit(): void {


    if(Object.keys(this.ques.e).length === 0 && this.ques.e.constructor === Object){
      this.navigateTo('welcome');
      return;
    }

    this.response = this.ques.evaluate();

    let data = {
      choices: this.ques.getEvalue(),
      report: this.response
    }

    this.network.postUserReport(data).then( v => {
    });


  }



}
