function generateQuestionnaire() {


    var t = {
        phase: "user_phase",
        form: {
            sections: [{
                localizedLabel: "تشخيص أعراض فيروس كورونا",
                key: "user_single_choice",
                answer: {
                    type: "SingleChoice",
                    choices: [{
                        localizedLabel: "تشخيص لي",
                        value: !0
                    }, {
                        localizedLabel: "تشخيص لشخص آخر",
                        value: !1
                    }]
                },
                isOptional: !1
            }]
        }
    };

    function a(e) {
        return {
            phase: "emergency_phase",
            form: {
                sections: [{
                    icon: "asterisk.circle.fill",
                    localizedLabel: "قبل أن تبدأ",
                    detailedLocalizedLabel: e ? "إذا كنت تعاني أي من هذه الأعراض فعليك التوقف مباشرة والاتصال بأرقام الطوارئ في بلدك!" : "إذا كنت تعاني أي من هذه الأعراض فعليك التوقف مباشرة والاتصال بأرقام الطوارئ في بلدك!",
                    key: "emergency_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: e ? "لدي واحدة على الأقل من هذه الأعراض" : "يعاني أحد هذه الأعراض على الأقل",
                            value: !0
                        }, {
                            localizedLabel: e ? "ليست لدي أي من هذه الأعراض" : "لا يعاني أي من هذه الأعراض",
                            value: !1
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function i(e) {
        return {
            phase: "age_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "كم عمرك؟" : "كم العمر؟",
                    key: "age_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: "أقل من 18 سنه",
                            value: 0
                        }, {
                            localizedLabel: "مابين 18 و 64 سنه",
                            value: 1
                        }, {
                            localizedLabel: "65 سنة أو أكبر",
                            value: 2
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function s(e) {
        return {
            phase: "symptom_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "هل تعاني أحد هذه الأعراض؟" : "هل يعاني أحد هذه الأعراض؟",
                    detailedLocalizedLabel: "اختر الأعراض المطابقة للحالة",
                    key: "symptom_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 6,
                        choices: [{
                            localizedLabel: "حمى أو قشعريرة أو تعرق",
                            value: 0
                        }, {
                            localizedLabel: "صعوبة في التنفس (ليست شديدة)",
                            value: 1
                        }, {
                            localizedLabel: "سعال حديث أو متفاقم",
                            value: 2
                        }, {
                            localizedLabel: "إلتهاب الحلق",
                            value: 3
                        }, {
                            localizedLabel: "آلام عامة بالجسد",
                            value: 4
                        }, {
                            localizedLabel: "القيء أو الإسهال",
                            value: 5
                        }, {
                            localizedLabel: "لا شيء مما سبق",
                            isExclusive: !0,
                            value: 6
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function n(e) {
        return {
            phase: "exposure_place_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "في آخر 14 يومًا، هل كنت في منطقة ينتشر بها فيروس كورونا على نطاق واسع؟" : "في آخر 14 يومًا ، هل كان في منطقة ينتشر بها فيروس كورونا على نطاق واسع؟",
                    detailedLocalizedLabel: "اختر الخيار أو الخيارات المناسبة",
                    key: "exposure_place_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 4,
                        choices: [{
                            localizedLabel: e ? "أعيش في منطقة ينتشر بها وباء كورونا على نطاق واسع" : "يعيش في منطقة ينتشر بها وباء كورونا على نطاق واسع",
                            value: 0
                        }, {
                            localizedLabel: e ? "لقد زرت منطقة ينتشر فيها وباء كورونا على نطاق واسع" : " زار منطقة ينتشر فيها وباء كورونا على نطاق واسع",
                            value: 1
                        }, {
                            localizedLabel: "لا أدري",
                            isExclusive: !0,
                            value: 2
                        }, {
                            localizedLabel: "لا شيء مما سبق",
                            isExclusive: !0,
                            value: 3
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function r(e) {
        return {
            phase: "exposure_person_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "في آخر 14 يومًا، ما هي درجة تعرضك للآخرين الذين تم تشخيصهم بوباء كورونا ؟" : "في آخر 14 يومًا، ما هي درجة تعرضه للآخرين الذين تم تشخيصهم بوباء كورونا؟",
                    detailedLocalizedLabel: "اختر الخيار أو الخيارات المناسبة",
                    key: "exposure_person_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 4,
                        choices: [{
                            localizedLabel: e ? "أعيش مع شخص لديه وباء كورونا" : "يعيش مع شخص لديه وباء كورونا",
                            value: 0
                        }, {
                            localizedLabel: e ? "كان لي تواصل وثيق مع شخص لديه وباء كورونا" : "كان له تواصل وثيق مع شخص لديه وباء كورونا",
                            detailedLocalizedLabel: e ? "كنت على بعد مترين من شخص مريض لمدة 10 دقائق،أو تعرضت لسعاله وعطسه" : "كان على بعد مترين من شخص مريض لمدة 10 دقائق،أو تعرض لسعاله وعطسه",
                            value: 1
                        }, {
                            localizedLabel: e ? " كنت بالقرب من شخص لديه وباء كورونا" : "كان بالقرب من شخص لديه وباء كورونا",
                            detailedLocalizedLabel: e ? "كنت على بعد مترين على الأقل ولم أتعرض لعطس أو سعال" : "Tكان على بعد مترين على الأقل ولم يتعرض لعطس أو سعال",
                            value: 2
                        }, {
                            localizedLabel: e ? "لم يكن لدي أي تعرض لوباء كورونا" : "لم يكن لديه أي تعرض لوباء كورونا",
                            detailedLocalizedLabel: e ? "لم أكن على اتصال بشخص مريض" : "لم يكن على اتصال بشخص مريض",
                            isExclusive: !0,
                            value: 3
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function l(e) {
        return {
            phase: "travel_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "هل سافرت خارج البلاد خلال الـ 14 يومًا الماضية؟" : "هل سافر خارج البلاد خلال الـ 14 يومًا الماضية؟",
                    key: "travel_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: e ? "نعم سافرت خاج البلاد" : "نعم سافر خارج البلاد",
                            value: !0
                        }, {
                            localizedLabel: e ? "لا، لم أسافر خارج البلاد" : "لا، لم يسافر خارج البلاد",
                            value: !1
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function h(e) {
        return {
            phase: "condition_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "هل لديك أي حالة صحية أخرى؟" : "هل لديه أي حالة صحية أخرى؟",
                    detailedLocalizedLabel: "اختر الخيار أو الخيارات المناسبة",
                    key: "condition_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 9,
                        choices: [{
                            localizedLabel: "الربو أو أمراض الرئة المزمنة",
                            value: 0
                        }, {
                            localizedLabel: "حامل",
                            value: 1
                        }, {
                            localizedLabel: "مرض السكري مع مضاعفات",
                            value: 2
                        }, {
                            localizedLabel: "الأمراض أو الظروف الصحية التي تجعل من الصعب السعال",
                            value: 3
                        }, {
                            localizedLabel: "فشل كلوي يحتاج لغسيل الكلى",
                            value: 4
                        }, {
                            localizedLabel: "تليف الكبد",
                            value: 5
                        }, {
                            localizedLabel: "ضعف جهاز المناعة",
                            value: 6
                        }, {
                            localizedLabel: "فشل القلب الاحتقاني",
                            value: 7
                        }, {
                            localizedLabel: "بدانة مفرطة",
                            value: 8
                        }, {
                            localizedLabel: "لا شيء مما سبق",
                            isExclusive: !0,
                            value: 9
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function c(e) {
        return {
            phase: "care_facility_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "هل تعيش أو تعمل في مركز رعاية صحية؟" : "هل يعيش أو يعمل في مركز رعاية صحية؟",
                    detailedLocalizedLabel: "وهذا يشمل مستشفى أو غرفة طوارئ أو مركز طبي آخر أو مرفق طويل الأمد",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: e ? "أعيش في مركز رعاية طويلة الأمد" : "يعيش في مركز رعاية طويلة الأمد",
                            detailedLocalizedLabel: "وهذا يشمل دور التمريض أو الرعاية الصحية المساعدة",
                            value: 0
                        }, {
                            localizedLabel: e ? "عملت في مستشفى أو مرفق رعاية  خلال الـ 14 يومًا الماضية" : "عمل في مستشفى أو مرفق رعاية  خلال الـ 14 يومًا الماضية",
                            detailedLocalizedLabel: "وهذا يشمل التطوع والمساعدة",
                            value: 1
                        }, {
                            localizedLabel: e ? "أخطط للعمل في مستشفى أو مرفق رعاية خلال الـ 14 يومًا القادمة" : "يخطط للعمل في مستشفى أو مرفق رعاية خلال الـ 14 يومًا القادمة",
                            detailedLocalizedLabel: "وهذا يشمل التطوع والمساعدة",
                            value: 2
                        }, {
                            localizedLabel: e ? "لا، أنا لا أعيش أو أعمل في مركز رعاية طبي" : "لا ، هو لا أعيش أو يعمل في مركز رعاية طبي",
                            value: 3
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function k(e) {
        return {
            title: "لا يوجد اختبار في هذا الوقت",
            detail: e ? "حتى الآن، تشير إجاباتك إلى أنك لست بحاجة إلى إجراء اختبار. إذا تغير أي شيء، استخدم التطبيق مرة أخرى." : "حتى الآن، تشير إجاباتك إلى أنه ليس بحاجة إلى إجراء اختبار. إذا تغير أي شيء، استخدم التطبيق مرة أخرى."
        }
    }

    function d(e) {
        return {
            title: "راقب الأعراض",
            detail: e ? "راقب أعراض وباء كورونا مثل السعال والحمى وصعوبة التنفس. إذا تفاقمت الأعراض، فاتصل بطبيبك." : "راقب أعراض وباء كورونا مثل السعال والحمى وصعوبة التنفس. إذا تفاقمت الأعراض، فاتصل بطبيبه."
        }
    }

    function o(e) {
        return {
            title: "قم بالتنسيق للحصول على اختبار",
            detail: e ? "تشير إجاباتك إلى أنك قد تحتاج إلى إجراء اختبار لمرض كورونا. يجب عليك الاتصال بمكتب طبيبك أو قسم الصحة المحلي أو التابع لمدينتك للحصول على مزيد من المعلومات. \n يجب عليك البقاء هدائاً حتى توفر الفحص." : "تشير إجاباتك إلى أنه قد يحتاج إلى إجراء اختبار لمرض كورونا. يجب عليك الاتصال بمكتب طبيبك أو قسم الصحة المحلي أو التابع لمدينتك للحصول على مزيد من المعلومات. \n يجب عليه البقاء هدائاً حتى توفر الفحص."
        }
    }

    function T(e) {
        return {
            title: e ? "تواصل مع الجهات المختصة بالفحص " : "تواصل مع الجهات المختصة بالفحص ",
            detail: e ? "يجب عليك إخطار جهة عملك والجهات المختصة بأعراضك الحالية بأسرع ما يمكن. هذا أمر حيوي لإبطاء انتشار وباء الكورونا." : "يجب عليك إخطار جهة عمله والجهات المختصة بأعراضه الحالية بأسرع ما يمكن. هذا أمر حيوي لإبطاء انتشار وباء الكورونا."
        }
    }

    function I(e) {
        return {
            title: "راقب الأعراض",
            detail: e ? "راقب أعراض وباء كورونا  مثل السعال والحمى وصعوبة التنفس. إذا تفاقمت الأعراض ، فاتصل بطبيبك." : "راقب أعراض وباء كورونا  مثل السعال والحمى وصعوبة التنفس. إذا تفاقمت الأعراض ، فاتصل بطبيبه."
        }
    }

    function C(e) {
        return {
            title: "قم بالعزل الذاتي عن الآخرين",
            detail: e ? "يجب أن تحاول الابتعاد عن الآخرين لمدة 7 أيام على الأقل من ظهور الأعراض لأول مرة. يمكن أن تنتهي العزلة إذا تحسنت أعراضك بشكل ملحوظ وإذا لم يكن لديك حمى لمدة 72 ساعة على الأقل دون استخدام الدواء. \n عن طريق عزل نفسك، يمكنك إبطاء انتشار وباء كورونا وحماية الآخرين." : "يجب أن يحاول الابتعاد عن الآخرين لمدة 7 أيام على الأقل من ظهور الأعراض لأول مرة. يمكن أن تنتهي العزلة إذا تحسنت أعراضه بشكل ملحوظ وإذا لم يكن لديه حمى لمدة 72 ساعة على الأقل دون استخدام الدواء. \n عن طريق عزله لنفسه، يمكنك إبطاء انتشار وباء كورونا وحماية الآخرين."
        }
    }

    function L() {
        return {
            title: "عليك بالراحة والعناية",
            detail: "تناول الطعام الصحي، واشرب السوائل، واحصل على قسط وافر من الراحة."
        }
    }

    function x() {
        return {
            title: "حافظ على مسافة التباعد الاجتماعي",
            detail: "يمكن للخطوات البسيطة ولكن المهمة  إبطاء انتشار وباء الكورونا . تجنَّب التجمعات للأشخاص وابتعد عنها مترين على الأقل مما ليسو من أفراد الأسرة. تجنب بشكل خاص من تظهر عليها أعراض المرض."
        }
    }

    function z(e) {
        return {
            title: "راقب الأعراض عن كثب",
            detail: e ? "راقب أعراض مرض كورونا  مثل السعال والحمى وصعوبة التنفس. تحقق أيضًا من درجة حرارتك مرتين يوميًا لمدة أسبوعين. إذا تفاقمت الأعراض، اتصل بطبيبك أو الجهات المختصة ." : "راقب أعراض مرض كورونا  مثل السعال والحمى وصعوبة التنفس. تحقق أيضًا من درجة حرارته مرتين يوميًا لمدة أسبوعين. إذا تفاقمت الأعراض، اتصل بطبيبك أو الجهات المختصة."
        }
    }

    function O(e) {
        return {
            artwork: 1,
            title: e ? "اتصل بفريق الرعاية بالمرفق" : "اتصل بفريق الرعاية بالمرفق",
            detail: e ? "بناءً على إجاباتك، يجب عليك التواصل مع الطبيب أو فريق الرعاية المسؤول في بالمرفق." : "بناءً على إجاباتك، يجب عليك التواصل مع الطبيب أو فريق الرعاية المسؤول في بالمرفق.",
            needs: [{
                title: (o = e) ? "اتصل بطبيبك أو فريق الرعاية." : "اتصل بطبيبه أو فريق الرعاية.",
                detail: o ? "يجب مناقشة الأعراض التي تعانيها مع الأطباء أو فريق الرعاية الذي يعتني بالمرفق الخاص بك. \n قد يختلف وقت استجابة طبيبك اعتمادًا على عدد الحالات في منطقتك." : "يجب مناقشة الأعراض التي يعانيها مع الأطباء أو فريق الرعاية الذي يعتني بالمرفق الخاص به. \n قد يختلف وقت استجابة طبيبك اعتمادًا على عدد الحالات في منطقتك."
            }, C(e), L(), d(e)],
            nonNeeds: []
        };
        var o
    }

    function D(e) {
        return {
            artwork: 2,
            title: e ? "اتصل بمسؤل الخدمات الصحية بمقر عملك" : "اتصل بمسؤل الخدمات الصحية بمقر عمله",
            detail: e ? "يجب أن تخبر عملك عن أعراضك الحالية في أقرب وقت ممكن." : "يجب أن تخبر عمله عن أعراضه الحالية في أقرب وقت ممكن.",
            needs: [C(e), L(), T(e), I(e), o(e)],
            nonNeeds: []
        }
    }

    function S(e) {
        return {
            artwork: 2,
            title: e ? "اتصل بمزود الخدمات الصحية " : "اتصل بمزود الخدمات الصحية ",
            detail: e ? "تشير إجاباتك إلى أنه يجب عليك التحدث إلى أخصائي طبي حول إجراء اختبار لفحص كورونا." : "تشير إجاباتك إلى أنه يجب عليك التحدث إلى أخصائي طبي حول إجراء اختبار لفحص كورونا.",
            needs: [C(e), L(), o(e), d(e)],
            nonNeeds: []
        }
    }

    function Y(e) {
        return {
            artwork: 4,
            title: e ? "عليك عزل نفسك ذاتياً" : "عليه عزل نفسه",
            detail: e ? "بناءً على إجاباتك ، يجب أن تبقى في المنزل بعيدًا عن الآخرين. إذا أمكنك، احصل على غرفة وحمام خاص بك فقط. \n قد يكون هذا صعبًا عندما لا تشعر أنك بحالة جيدة، ولكنه سيحمي من حولك." : "بناءً على إجاباتك، يجب أن يبقى في المنزل بعيدًا عن الآخرين. إذا أمكن خصص  له غرفة وحمام خاص به. \n قد يكون هذا صعبًا نفسياً له مع حالة المرض ولكنه سيحمي من حوله.",
            needs: [C(e), L(), d(e)],
            nonNeeds: [k(e)]
        }
    }

    function V(e) {
        return {
            artwork: 4,
            title: e ? "عليك عزل نفسك ذاتياً" : "عليه عزل نفسه",
            detail: e ? "بناءً على إجاباتك، يجب أن تبقى في المنزل بعيدًا عن الآخرين. إذا أمكنك، احصل على غرفة وحمام خاص بك فقط. \n قد يكون هذا صعبًا عندما لا تشعر أنك بحالة جيدة، ولكنه سيحمي من حولك." : "بناءً على إجاباتك، يجب أن يبقى في المنزل بعيدًا عن الآخرين. إذا أمكن خصص  له غرفة وحمام خاص به. \n قد يكون هذا صعبًا نفسياً له مع حالة المرض ولكنه سيحمي من حوله.",
            needs: [C(e), L(), d(e)],
            nonNeeds: []
        }
    }

    function N(e, o, t) {
        return {
            artwork: 3,
            title: e ? "حافظ على مسافة التباعد الاجتماعي" : "عليه المحافظة على مسافة التباعد الاجتماعي",
            detail: e ? "ساهم في إبطاء انتشار وباء الكورونا. تجنَّب التجمعات للأشخاص وابتعد عن الآخرين مسافة  مترين على الأقل و ينبغي أن لا تستخدم المواصلات العامة إلا للضرورة القصوى." : "ساهم في إبطاء انتشار وباء الكورونا. دعه يتجنَّب التجمعات للأشخاص والابتعادد عن الآخرين مسافة  مترين على الأقل و ينبغي أن لا يستخدم المواصلات العامة إلا للضرورة القصوى.",
            needs: [x()].concat(o || t ? [{
                title: (a = e) ? "احتفظ بمخزون من دوائك" : "احتفظ بمخزون من دوائه",
                detail: a ? "إن كنت تتناول دواء ما فعليك التشاور مع طبيبك بخصوص جرعة تكفي لـ30 يوماً" : "إن كان تتناول دواء ما فعليه التشاور مع طبيبه بخصوص جرعة تكفي لـ30 يوماً"
            }] : []),
            nonNeeds: [k(e)]
        };
        var a
    }

    function u(e) {
        return [t, a(e), i(e), s(e), h(e), l(e), n(e), r(e), c(e)]
    }
    return {
        identifier: "com.acwellness.questionnaire.covid-19_symptom_assessment",
        title: "تقييم أعراض مرض كورونا",
        description: "التقييم الشخصي لأعراض مرض كورونا",
        version: "1.3.0",
        region: "us",
        user_phase(){
          return t;
        },
        emergency_phase(e){
          return a(e);
        },
        age_phase(e){
          return i(e);
        },
        symptom_phase(e){
          return  s(e);
        },
        condition_phase(e){
          return h(e);
        },
        travel_phase(e){
          return l(e);
        },
        exposure_place_phase(e){
          return n(e);
        },
        exposure_person_phase(e){
          return r(e);
        },
        care_facility_phase(e){
            return c(e);
        },
        report(e){
          return u(e);
        },

        function: function(e) {
            var o = !("user_single_choice" in e.responses) || e.responses.user_single_choice;
            return "initial" === e.phase ? t : "user_phase" === e.phase ? a(o) : "emergency_phase" === e.phase ? ("emergency_single_choice" in e.responses ? !0 === e.responses.emergency_single_choice : void 0) ? {
                phase: "done"
            } : i(o) : "age_phase" === e.phase ? 0 === e.responses.age_single_choice ? {
                phase: "done"
            } : s(o) : "symptom_phase" === e.phase ? h(o) : "condition_phase" === e.phase ? l(o) : "travel_phase" === e.phase ? n(o) : "exposure_place_phase" === e.phase ? r(o) : "exposure_person_phase" === e.phase ? c(o) : {
                phase: "done"
            }
        },
        evaluator: function(e) {
            var o = [],
                t = !("user_single_choice" in e) || e.user_single_choice,
                a = "travel_single_choice" in e ? !0 === e.travel_single_choice : void 0,
                i = "exposure_person_multiple_choice" in e ? -1 !== e.exposure_person_multiple_choice.indexOf(0) || -1 !== e.exposure_person_multiple_choice.indexOf(1) : void 0,
                s = "exposure_place_multiple_choice" in e ? -1 !== e.exposure_place_multiple_choice.indexOf(0) || -1 !== e.exposure_place_multiple_choice.indexOf(1) : void 0,
                n = "exposure_place_multiple_choice" in e ? -1 !== e.exposure_place_multiple_choice.indexOf(3) : void 0,
                r = a || s || i,
                l = "symptom_multiple_choice" in e ? -1 !== e.symptom_multiple_choice.indexOf(6) : void 0,
                h = "symptom_multiple_choice" in e ? !(l || -1 !== e.symptom_multiple_choice.indexOf(0) || -1 !== e.symptom_multiple_choice.indexOf(1) || -1 !== e.symptom_multiple_choice.indexOf(2)) : void 0,
                c = "condition_multiple_choice" in e ? !(-1 !== e.condition_multiple_choice.indexOf(9)) : void 0,
                d = "care_facility_single_choice" in e ? 1 === e.care_facility_single_choice || 2 === e.care_facility_single_choice : void 0,
                u = "care_facility_single_choice" in e ? 0 === e.care_facility_single_choice : void 0,
                y = "age_single_choice" in e ? 2 === e.age_single_choice : void 0;
            l ? o.push({
                title: t ? "لا تعاني من أي أعراض" : "لا يعاني من أي من الأعراض",
                icon: "thermometer"
            }) : o.push({
                title: t ? "أنت تعاني من أعراض" : "هو يعاني من أعرض",
                icon: "thermometer"
            }), c ? o.push({
                title: t ? "لديك حالات صحية ذات علاقة" : "لدية حالات صحية ذات علاقة",
                icon: "person.crop.circle"
            }) : o.push({
                title: t ? "ليس لديك حالات صحية ذات صلة" : "ليس لديه حلات صحية ذات صلة",
                icon: "person.crop.circle"
            }), a ? o.push({
                title: t ? "قمت بالسفر خارج البلاد" : "قام بالسفر خارج البلاد",
                icon: "location"
            }) : o.push({
                title: t ? "لم تقم بالسفر خارج البلاد" : "لم يقم بالسفر خارج البلاد",
                icon: "location"
            }), s ? o.push({
                title: t ? "كنت بمنطقة ينتشر فيها الوباء" : "كان بمنطقة ينتشر بها الوباء",
                icon: "globe"
            }) : n && o.push({
                title: t ? "لم تكن بمنطقة ينتشر بها الوباء" : "لم يكن بمنطقة ينتشر بها الوباء",
                icon: "globe"
            }), i ? o.push({
                title: t ? "خالطت اشخاصاً مصابين" : "خالط أشخاصاً مصابين",
                icon: "person.2"
            }) : o.push({
                title: t ? "لم تخالط أشخاصاً مصابين" : "لم يخالط أشخاصاً مصابين",
                icon: "person.2"
            }), d || u ? o.push({
                title: t ? "تعيش أو تعمل بمركز إيواء" : "يعيش أو يعمل بمركز إيواء",
                icon: "house"
            }) : o.push({
                title: t ? "لا تعيش ولا تعمل بمركز إيواء" : "لا يعيش ولا يعمل بمركز إيواء",
                icon: "house"
            });
            var p = {};
            p.formattedResponses = o, p.isForUser = t;
            var m, f, v, w, g, _ = "emergency_single_choice" in e ? !0 === e.emergency_single_choice : void 0,
                b = "age_single_choice" in e ? 0 === e.age_single_choice : void 0;
            return _ ? p.stop = {
                icon: "asterisk.circle.fill",
                title: (g = t) ? "يجب الاتصال برقم الطوارئ" : "يجب الاتصال برقم الطوارئ",
                detail: g ? "بناءً على الأعراض التي أبلغت عنها، يجب عليك طلب الرعاية على الفور." : "بناءً على الأعراض التي أبلغت عنها، يجب عليك طلب الرعاية على الفور.",
                actions: [{
                    type: "CallNumber",
                    title: "Call 911",
                    number: "911",
                    isEmphasized: !0
                }]
            } : b ? p.stop = {
                title: "هذا الفحص مخصص للأشخاص الذين لا يقل عمرهم عن 18 سنة",
                detail: "قم بزيارة موقع المركز الأمريكي لمكافحة الأوبئة والوقاية منها  CDC للحصول على معلومات حول وباء كورونا للأشخاص الأصغر سنًا.",
                actions: [{
                    type: "OpenURL",
                    title: "لمزيد من المعلومات قم بزيارة موقع المركز ",
                    url: "https://www.cdc.gov/coronavirus/2019-ncov/prepare/children.html"
                }]
            } : p.care = (w = null, {
                summary: {
                    artwork: (w = l ? d ? a || i ? {
                        artwork: 2,
                        title: (f = t) ? "اتصل بمسؤل الخدمات الصحية بمقر عملك" : "عليه الاتصال بمسؤل الخدمات الصحية بمقر عمله",
                        detail: f ? "كعامل أساسي ، يجب عليك الاتصال بمسؤل الخدمات الصحية، وحماية نفسك في العمل، وممارسة التباعد الاجتماعي خارج العمل." : "كعامل أساسي، يجب عليه الاتصال بمسؤل الخدمات الصحية، وحماية نفسه في العمل، وممارسة التباعد الاجتماعي خارج العمل.",
                        needs: [{
                            title: "اتخذ الاحتياطات اللازمة لحماية الآخرين",
                            detail: f ? "قد تحتاج إلى ارتداء قناع للمساعدة في حماية نفسك ومن حولك." : "قد يحتاج إلى ارتداء قناع للمساعدة في حماية نفسه ومن حوله."
                        }, x(), z(f)],
                        nonNeeds: [k(f)]
                    } : N(t, y, c) : r ? {
                        artwork: 3,
                        title: (m = t) ? "حافظ على مسافة التباعد الاجتماعي" : "عليه المحافظة على مسافة التباعد الاجتماعي",
                        detail: m ? "ساهم في إبطاء انتشار وباء الكورونا. تجنَّب التجمعات للأشخاص وابتعد عن الآخرين مسافة مترين على الأقل و ينبغي أن لا تستخدم المواصلات العامة إلا للضرورة القصوى." : "ساهم في إبطاء انتشار وباء الكورونا. دعه يتجنَّب التجمعات للأشخاص والابتعادد عن الآخرين مسافة  مترين على الأقل و ينبغي أن لا يستخدم المواصلات العامة إلا للضرورة القصوى.",
                        needs: [{
                            title: "قم بالحجر المنزلي",
                            detail: m ? "ربما تكون قد تعرضت للفيروس. يجب أن تبقى في المنزل لمدة 14 يومًا القادمة ومعرفة ما إذا ظهرت أي أعراض. \n يجب عليك أيضًا محاولة تقييد الاتصال مع الآخرين خارج المنزل." : "ربما يكون قد تعرض للفيروس. يجب أن يبقى في المنزل لمدة 14 يومًا القادمة ومعرفة ما إذا ظهرت أي أعراض. \n يجب عليه أيضًا محاولة تقييد الاتصال مع الآخرين خارج المنزل."
                        }, z(m)],
                        nonNeeds: [k(m)]
                    } : N(t, y, c) : r ? (u ? O : d ? D : c || y && !h ? S : !y && h ? Y : V)(t) : d && !l ? h ? {
                        artwork: 2,
                        title: (v = t) ? "اتصل بمسؤل الخدمات الصحية بمقر عملك" : "اتصل بمسؤل الخدمات الصحية بمقر عمله",
                        detail: v ? "يجب أن تخبر عملك عن أعراضك الحالية في أقرب وقت ممكن." : "يجب أن يخبر عمله عن أعراضه الحالية في أقرب وقت ممكن.",
                        needs: [C(v), L(), T(v), I(v)],
                        nonNeeds: []
                    } : D(t) : (u ? O : y ? c ? h ? V : S : h ? Y : V : c ? h ? V : S : Y)(t)).artwork,
                    title: w.title,
                    detail: w.detail
                },
                needs: w.needs,
                nonNeeds: w.nonNeeds
            }), p
        },
        progress: function(e) {
            var o = !("user_single_choice" in e.responses) || e.responses.user_single_choice,
                t = e.phase,
                a = e.responses,
                i = 1 / u(o).length;
            if ("initial" === t) return 0;
            if ("done" === t) return 1;
            var s = u(o).map(function(e) {
                return e.phase
            }).indexOf(t);
            return s * i + function(e, o) {
                for (var t = 0, a = 0; a < o.length; a++) o[a] in e && (t += 1);
                return t / o.length
            }(a, u(o)[s].form.sections.map(function(e) {
                return e.key
            })) * i
        }
    }
}
