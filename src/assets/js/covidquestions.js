function generateQuestionnaire() {


    var t = {
        phase: "user_phase",
        form: {
            sections: [{
                localizedLabel: "COVID-19 Screening Tool",
                key: "user_single_choice",
                answer: {
                    type: "SingleChoice",
                    choices: [{
                        localizedLabel: "Use For Myself",
                        value: !0
                    }, {
                        localizedLabel: "Use For Someone Else",
                        value: !1
                    }]
                },
                isOptional: !1
            }]
        }
    };

    function a(e) {
        return {
            phase: "emergency_phase",
            form: {
                sections: [{
                    icon: "asterisk.circle.fill",
                    localizedLabel: "Before You Start",
                    detailedLocalizedLabel: e ? "If you are experiencing any of these symptoms, stop and call 911.\n\nâ€¢   Constant chest pain or pressure\nâ€¢   Extreme difficulty breathing\nâ€¢   Severe, constant dizziness or lightheadedness\nâ€¢   Slurred speech\nâ€¢   Difficulty waking up" : "If they are experiencing any of these symptoms, stop and call 911.\n\nâ€¢   Constant chest pain or pressure\nâ€¢   Extreme difficulty breathing\nâ€¢   Severe, constant dizziness or lightheadedness\nâ€¢   Slurred speech\nâ€¢   Difficulty waking up",
                    key: "emergency_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: e ? "I have at least one of these symptoms" : "They have at least one of these symptoms",
                            value: !0
                        }, {
                            localizedLabel: e ? "I do not have any of these symptoms" : "They do not have any of these symptoms",
                            value: !1
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function i(e) {
        return {
            phase: "age_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "How old are you?" : "How old are they?",
                    key: "age_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: "Under 18",
                            value: 0
                        }, {
                            localizedLabel: "Between 18 and 64",
                            value: 1
                        }, {
                            localizedLabel: "65 or older",
                            value: 2
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function s(e) {
        return {
            phase: "symptom_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "Are you experiencing any of these symptoms?" : "Are they experiencing any of these symptoms?",
                    detailedLocalizedLabel: "Select all that apply",
                    key: "symptom_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 6,
                        choices: [{
                            localizedLabel: "Fever, chills, or sweating",
                            value: 0
                        }, {
                            localizedLabel: "Difficulty breathing (not severe)",
                            value: 1
                        }, {
                            localizedLabel: "New or worsening cough",
                            value: 2
                        }, {
                            localizedLabel: "Sore throat",
                            value: 3
                        }, {
                            localizedLabel: "Aching throughout the body",
                            value: 4
                        }, {
                            localizedLabel: "Vomiting or diarrhea",
                            value: 5
                        }, {
                            localizedLabel: "None of the above",
                            isExclusive: !0,
                            value: 6
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function n(e) {
        return {
            phase: "exposure_place_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "In the last 14 days, have you been in an area where COVID-19 is widespread?" : "In the last 14 days, have they been in an area where COVID-19 is widespread?",
                    detailedLocalizedLabel: "Select all that apply",
                    key: "exposure_place_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 4,
                        choices: [{
                            localizedLabel: e ? "I live in an area where COVID-19 is widespread" : "They live in an area where COVID-19 is widespread",
                            value: 0
                        }, {
                            localizedLabel: e ? "I have visited an area where COVID-19 is widespread" : "They have visited an area where COVID-19 is widespread",
                            value: 1
                        }, {
                            localizedLabel: "I donâ€™t know",
                            isExclusive: !0,
                            value: 2
                        }, {
                            localizedLabel: "None of the above",
                            isExclusive: !0,
                            value: 3
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function r(e) {
        return {
            phase: "exposure_person_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "In the last 14 days, what is your exposure to others who are known to have COVID-19?" : "In the last 14 days, what is their exposure to others who are known to have COVID-19?",
                    detailedLocalizedLabel: "Select all that apply",
                    key: "exposure_person_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 4,
                        choices: [{
                            localizedLabel: e ? "I live with someone who has COVID-19" : "They live with someone who has COVID-19",
                            value: 0
                        }, {
                            localizedLabel: e ? "Iâ€™ve had close contact with someone who has COVID-19" : "Theyâ€™ve had close contact with someone who has COVID-19",
                            detailedLocalizedLabel: e ? "For 10 minutes, I was within 6 feet of someone whoâ€™s sick or exposed to a cough or sneeze." : "For 10 minutes, they were within 6 feet of someone whoâ€™s sick or exposed to a cough or sneeze.",
                            value: 1
                        }, {
                            localizedLabel: e ? "Iâ€™ve been near someone who has COVID-19" : "Theyâ€™ve been near someone who has COVID-19",
                            detailedLocalizedLabel: e ? "I was at least 6 feet away and was not exposed to a sneeze or cough." : "They were at least 6 feet away and were not exposed to a sneeze or cough.",
                            value: 2
                        }, {
                            localizedLabel: e ? "Iâ€™ve had no exposure" : "Theyâ€™ve had no exposure",
                            detailedLocalizedLabel: e ? "I have not been in contact with someone whoâ€™s sick." : "They have not been in contact with someone whoâ€™s sick.",
                            isExclusive: !0,
                            value: 3
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function l(e) {
        return {
            phase: "travel_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "In the last 14 days, have you traveled internationally?" : "In the last 14 days, have they traveled internationally?",
                    key: "travel_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: e ? "I have traveled internationally" : "They have traveled internationally",
                            value: !0
                        }, {
                            localizedLabel: e ? "I have not traveled internationally" : "They have not traveled internationally",
                            value: !1
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function h(e) {
        return {
            phase: "condition_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "Do you have any of these conditions?" : "Do they have any of these conditions?",
                    detailedLocalizedLabel: "Select all that apply",
                    key: "condition_multiple_choice",
                    answer: {
                        type: "MultipleChoice",
                        maximumNumberOfChoices: 9,
                        choices: [{
                            localizedLabel: "Asthma or chronic lung disease",
                            value: 0
                        }, {
                            localizedLabel: "Pregnancy",
                            value: 1
                        }, {
                            localizedLabel: "Diabetes with complications",
                            value: 2
                        }, {
                            localizedLabel: "Diseases or conditions that make it harder to cough",
                            value: 3
                        }, {
                            localizedLabel: "Kidney failure that needs dialysis",
                            value: 4
                        }, {
                            localizedLabel: "Cirrhosis of the liver",
                            value: 5
                        }, {
                            localizedLabel: "Weakened immune system",
                            value: 6
                        }, {
                            localizedLabel: "Congestive heart failure",
                            value: 7
                        }, {
                            localizedLabel: "Extreme obesity",
                            value: 8
                        }, {
                            localizedLabel: "None of the above",
                            isExclusive: !0,
                            value: 9
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function c(e) {
        return {
            phase: "care_facility_phase",
            form: {
                sections: [{
                    localizedLabel: e ? "Do you live or work in a care facility?" : "Do they live or work in a care facility?",
                    detailedLocalizedLabel: "This includes a hospital, emergency room, other medical setting, or long-term facility.",
                    key: "care_facility_single_choice",
                    answer: {
                        type: "SingleChoice",
                        choices: [{
                            localizedLabel: e ? "I live in a long-term care facility" : "They live in a long-term care facility",
                            detailedLocalizedLabel: "This includes nursing homes or assisted living.",
                            value: 0
                        }, {
                            localizedLabel: e ? "I have worked in a hospital or other care facility in the past 14 days" : "They have worked in a hospital or other care facility in the past 14 days",
                            detailedLocalizedLabel: "This includes volunteering.",
                            value: 1
                        }, {
                            localizedLabel: e ? "I plan to work in a hospital or other care facility in the next 14 days" : "They plan to work in a hospital or other care facility in the next 14 days",
                            detailedLocalizedLabel: "This includes volunteering.",
                            value: 2
                        }, {
                            localizedLabel: e ? "No, I donâ€™t live or work in a care facility" : "No, they donâ€™t live or work in a care facility",
                            value: 3
                        }]
                    },
                    isOptional: !1
                }]
            }
        }
    }

    function k(e) {
        return {
            title: "No Test at This Time",
            detail: e ? "As of now, your answers suggest you do not need to get tested. If anything changes, take the questionnaire again." : "As of now, their answers suggest they do not need to get tested. If anything changes, take the questionnaire again."
        }
    }

    function d(e) {
        return {
            title: "Monitor Symptoms",
            detail: e ? "Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. If your symptoms get worse, contact your doctorâ€™s office." : "Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. If their symptoms get worse, contact their doctorâ€™s office."
        }
    }

    function o(e) {
        return {
            title: "Talk to Someone About Testing",
            detail: e ? "Your answers suggest you may need to get tested for COVID-19. You should get in touch with your doctorâ€™s office or your state or local health department for more information.\n\nTesting access may vary by location and provider." : "Their answers suggest they may need to get tested for COVID-19. They should get in touch with their doctorâ€™s office or their state or local health department for more information.\n\nTesting access may vary by location and provider."
        }
    }

    function T(e) {
        return {
            title: e ? "Call Your Work Health Provider" : "Call Their Work Health Provider",
            detail: e ? "You should notify your work of your current symptoms as quickly as you can. This is vital to slowing the spread of COVID-19." : "They should notify their work of their current symptoms as quickly as they can. This is vital to slowing the spread of COVID-19."
        }
    }

    function I(e) {
        return {
            title: "Monitor Symptoms",
            detail: e ? "Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. If your symptoms get worse, contact your doctorâ€™s office." : "Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. If their symptoms get worse, contact their doctorâ€™s office."
        }
    }

    function C(e) {
        return {
            title: "Isolate From Others",
            detail: e ? "You should try to stay away from others for at least 7 days from when your symptoms first appeared. Your isolation can end if your symptoms improve significantly and if you have had no fever for at least 72 hours without the use of medicine.\n\nBy isolating yourself, you can slow the spread of COVID-19 and protect others." : "They should try to stay away from others for at least 7 days from when their symptoms first appeared. Their isolation can end if their symptoms improve significantly and if they have had no fever for at least 72 hours without the use of medicine.\n\nBy isolating them, they can slow the spread of COVID-19 and protect others."
        }
    }

    function L() {
        return {
            title: "Rest and Take Care",
            detail: "Eat well, drink fluids, and get plenty of rest."
        }
    }

    function x() {
        return {
            title: "Maintain Social Distance",
            detail: "Small but important steps can slow the spread of COVID-19. Avoid groups of people and keep six feet apart from anyone whoâ€™s not part of the household. Especially avoid those showing symptoms."
        }
    }

    function z(e) {
        return {
            title: "Monitor Symptoms",
            detail: e ? "Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. Also, check your temperature twice a day for two weeks. If symptoms get worse, call your doctor." : "Watch for COVID-19 symptoms such as cough, fever, and difficulty breathing. Also, check their temperature twice a day for two weeks. If symptoms get worse, call their doctor."
        }
    }

    function O(e) {
        return {
            artwork: 1,
            title: e ? "Contact Your Facilityâ€™s Care Team" : "Contact Their Facilityâ€™s Care Team",
            detail: e ? "Based on your answers, you should reach out to the doctor or care team in charge at your facility." : "Based on their answers, they should reach out to the doctor or care team in charge at their facility.",
            needs: [{
                title: (o = e) ? "Call Your Doctor or Care Team" : "Call Their Doctor or Care Team",
                detail: o ? "You should discuss your symptoms with the doctors or care team that look after your facility.\n\nYour doctorâ€™s response time may vary depending on the number of cases in your region." : "They should discuss their symptoms with the doctors or care team that look after their facility.\n\nTheir doctorâ€™s response time may vary depending on the number of cases in your region."
            }, C(e), L(), d(e)],
            nonNeeds: []
        };
        var o
    }

    function D(e) {
        return {
            artwork: 2,
            title: e ? "Contact Your Workâ€™s Occupational Health Provider" : "Contact Their Workâ€™s Occupational Health Provider",
            detail: e ? "You should let your work know your current symptoms as soon as possible." : "They should let their work know their current symptoms as soon as possible.",
            needs: [C(e), L(), T(e), I(e), o(e)],
            nonNeeds: []
        }
    }

    function S(e) {
        return {
            artwork: 2,
            title: e ? "Contact Your Healthcare Provider" : "Contact Their Healthcare Provider",
            detail: e ? "Your answers suggest you should talk to a medical professional about getting tested for COVID-19." : "Their answers suggest they should talk to a medical professional about getting tested for COVID-19.",
            needs: [C(e), L(), o(e), d(e)],
            nonNeeds: []
        }
    }

    function Y(e) {
        return {
            artwork: 4,
            title: e ? "You Should Self-Isolate" : "They Should Self-Isolate",
            detail: e ? "Based on your answers, you should stay home and away from others. If you can, have a room and bathroom thatâ€™s just for you.\n\nThis can be hard when youâ€™re not feeling well, but it will protect those around you." : "Based on their answers, they should stay home and away from others. If they can, have a room and bathroom thatâ€™s just for them.\n\nThis can be hard when theyâ€™re not feeling well, but it will protect those around them.",
            needs: [C(e), L(), d(e)],
            nonNeeds: [k(e)]
        }
    }

    function V(e) {
        return {
            artwork: 4,
            title: e ? "You Should Self-Isolate" : "They Should Self-Isolate",
            detail: e ? "Based on your answers, you should stay home and away from others. If you can, have a room and bathroom thatâ€™s just for you.\n\nThis can be hard when youâ€™re not feeling well, but it will protect those around you." : "Based on their answers, they should stay home and away from others. If they can, have a room and bathroom thatâ€™s just for them.\n\nThis can be hard when theyâ€™re not feeling well, but it will protect those around them.",
            needs: [C(e), L(), d(e)],
            nonNeeds: []
        }
    }

    function N(e, o, t) {
        return {
            artwork: 3,
            title: e ? "You Should Practice Social Distancing" : "They Should Practice Social Distancing",
            detail: e ? "Help stop the spread. When outside the home, stay at least six feet away from other people, avoid groups, and only use public transit if necessary." : "Help stop the spread. When outside the home, have them stay at least six feet away from other people, avoid groups, and only use public transit if necessary.",
            needs: [x()].concat(o || t ? [{
                title: (a = e) ? "Ask About Your Medications" : "Ask About Their Medications",
                detail: a ? "If youâ€™re currently taking prescription medication, you should contact your doctorâ€™s office about getting a 30-day supply." : "If theyâ€™re currently taking prescription medication, they should contact their doctorâ€™s office about getting a 30-day supply."
            }] : []),
            nonNeeds: [k(e)]
        };
        var a
    }

    function u(e) {
        return [t, a(e), i(e), s(e), h(e), l(e), n(e), r(e), c(e)]
    }
    return {
        identifier: "com.acwellness.questionnaire.covid-19_symptom_assessment",
        title: "COVID-19 Symptom Assessment",
        description: "Personal Assessment of COVID-19 Symptoms",
        version: "1.3.0",
        region: "us",
        user_phase(){
          return t;
        },
        emergency_phase(e){
          return a(e);
        },
        age_phase(e){
          return i(e);
        },
        symptom_phase(e){
          return  s(e);
        },
        condition_phase(e){
          return h(e);
        },
        travel_phase(e){
          return l(e);
        },
        exposure_place_phase(e){
          return n(e);
        },
        exposure_person_phase(e){
          return r(e);
        },
        care_facility_phase(e){
            return c(e);
        },

        function: function(e) {
            var o = !("user_single_choice" in e.responses) || e.responses.user_single_choice;
            return "initial" === e.phase ? t : "user_phase" === e.phase ? a(o) : "emergency_phase" === e.phase ? ("emergency_single_choice" in e.responses ? !0 === e.responses.emergency_single_choice : void 0) ? {
                phase: "done"
            } : i(o) : "age_phase" === e.phase ? 0 === e.responses.age_single_choice ? {
                phase: "done"
            } : s(o) : "symptom_phase" === e.phase ? h(o) : "condition_phase" === e.phase ? l(o) : "travel_phase" === e.phase ? n(o) : "exposure_place_phase" === e.phase ? r(o) : "exposure_person_phase" === e.phase ? c(o) : {
                phase: "done"
            }
        },
        evaluator: function(e) {
            var o = [],
                t = !("user_single_choice" in e) || e.user_single_choice,
                a = "travel_single_choice" in e ? !0 === e.travel_single_choice : void 0,
                i = "exposure_person_multiple_choice" in e ? -1 !== e.exposure_person_multiple_choice.indexOf(0) || -1 !== e.exposure_person_multiple_choice.indexOf(1) : void 0,
                s = "exposure_place_multiple_choice" in e ? -1 !== e.exposure_place_multiple_choice.indexOf(0) || -1 !== e.exposure_place_multiple_choice.indexOf(1) : void 0,
                n = "exposure_place_multiple_choice" in e ? -1 !== e.exposure_place_multiple_choice.indexOf(3) : void 0,
                r = a || s || i,
                l = "symptom_multiple_choice" in e ? -1 !== e.symptom_multiple_choice.indexOf(6) : void 0,
                h = "symptom_multiple_choice" in e ? !(l || -1 !== e.symptom_multiple_choice.indexOf(0) || -1 !== e.symptom_multiple_choice.indexOf(1) || -1 !== e.symptom_multiple_choice.indexOf(2)) : void 0,
                c = "condition_multiple_choice" in e ? !(-1 !== e.condition_multiple_choice.indexOf(9)) : void 0,
                d = "care_facility_single_choice" in e ? 1 === e.care_facility_single_choice || 2 === e.care_facility_single_choice : void 0,
                u = "care_facility_single_choice" in e ? 0 === e.care_facility_single_choice : void 0,
                y = "age_single_choice" in e ? 2 === e.age_single_choice : void 0;
            l ? o.push({
                title: t ? "You are not experiencing symptoms" : "They are not experiencing symptoms",
                icon: "thermometer"
            }) : o.push({
                title: t ? "You are experiencing symptoms" : "They are experiencing symptoms",
                icon: "thermometer"
            }), c ? o.push({
                title: t ? "You have relevant conditions" : "They have relevant conditions",
                icon: "person.crop.circle"
            }) : o.push({
                title: t ? "You do not have relevant conditions" : "They do not have relevant conditions",
                icon: "person.crop.circle"
            }), a ? o.push({
                title: t ? "You have traveled internationally" : "They have traveled internationally",
                icon: "location"
            }) : o.push({
                title: t ? "You have not traveled internationally" : "They have not traveled internationally",
                icon: "location"
            }), s ? o.push({
                title: t ? "You have been to an area where itâ€™s widespread" : "They have been to an area where itâ€™s widespread",
                icon: "globe"
            }) : n && o.push({
                title: t ? "You have not been to an area where itâ€™s widespread" : "They have not been to an area where itâ€™s widespread",
                icon: "globe"
            }), i ? o.push({
                title: t ? "You have been exposed to others who are sick" : "They have been exposed to others who are sick",
                icon: "person.2"
            }) : o.push({
                title: t ? "You have not been exposed to others who are sick" : "They have not been exposed to others who are sick",
                icon: "person.2"
            }), d || u ? o.push({
                title: t ? "You live or work in a care facility" : "They live or work in a care facility",
                icon: "house"
            }) : o.push({
                title: t ? "You do not live or work in a care facility" : "They do not live or work in a care facility",
                icon: "house"
            });
            var p = {};
            p.formattedResponses = o, p.isForUser = t;
            var m, f, v, w, g, _ = "emergency_single_choice" in e ? !0 === e.emergency_single_choice : void 0,
                b = "age_single_choice" in e ? 0 === e.age_single_choice : void 0;
            return _ ? p.stop = {
                icon: "asterisk.circle.fill",
                title: (g = t) ? "You Should Call 911" : "They Should Call 911",
                detail: g ? "Based on your reported symptoms, you should seek care immediately." : "Based on their reported symptoms, they should seek care immediately.",
                actions: [{
                    type: "CallNumber",
                    title: "Call 911",
                    number: "911",
                    isEmphasized: !0
                }]
            } : b ? p.stop = {
                title: "This tool is intended for people who are at least 18 years old",
                detail: "Visit the CDC site to get information about COVID-19 and younger people.",
                actions: [{
                    type: "OpenURL",
                    title: "Learn More on cdc.gov",
                    url: "https://www.cdc.gov/coronavirus/2019-ncov/prepare/children.html"
                }]
            } : p.care = (w = null, {
                summary: {
                    artwork: (w = l ? d ? a || i ? {
                        artwork: 2,
                        title: (f = t) ? "Contact Your Workâ€™s Occupational Health Provider" : "Contact Their Workâ€™s Occupational Health Provider",
                        detail: f ? "As an essential worker, you should contact your occupational health provider, protect yourself at work, and practice social distancing outside of work." : "As an essential worker, they should contact their occupational health provider, protect themselves at work, and practice social distancing outside of work.",
                        needs: [{
                            title: "Take Precautions to Protect Others",
                            detail: f ? "You may need to wear a mask to help protect yourself and those around you." : "They may need to wear a mask to help protect themselves and those around them."
                        }, x(), z(f)],
                        nonNeeds: [k(f)]
                    } : N(t, y, c) : r ? {
                        artwork: 3,
                        title: (m = t) ? "You Should Practice Social Distancing" : "They Should Practice Social Distancing",
                        detail: m ? "Help stop the spread. When outside the home, stay at least six feet away from other people, avoid groups, and only use public transit if necessary." : "Help stop the spread. When outside the home, have them stay at least six feet away from other people, avoid groups, and only use public transit if necessary.",
                        needs: [{
                            title: "Quarantine at Home",
                            detail: m ? "You may have been exposed. You should stay home for the next 14 days and see if any symptoms appear.\n\nYou should also try to limit your contact with others outside the home." : "They may have been exposed. They should stay home for the next 14 days and see if any symptoms appear.\n\nThey should also try to limit their contact with others outside the home."
                        }, z(m)],
                        nonNeeds: [k(m)]
                    } : N(t, y, c) : r ? (u ? O : d ? D : c || y && !h ? S : !y && h ? Y : V)(t) : d && !l ? h ? {
                        artwork: 2,
                        title: (v = t) ? "Contact Your Workâ€™s Occupational Health Provider" : "Contact Their Workâ€™s Occupational Health Provider",
                        detail: v ? "You should let your work know your current symptoms as soon as possible." : "They should let their work know their current symptoms as soon as possible.",
                        needs: [C(v), L(), T(v), I(v)],
                        nonNeeds: []
                    } : D(t) : (u ? O : y ? c ? h ? V : S : h ? Y : V : c ? h ? V : S : Y)(t)).artwork,
                    title: w.title,
                    detail: w.detail
                },
                needs: w.needs,
                nonNeeds: w.nonNeeds
            }), p
        },
        progress: function(e) {
            var o = !("user_single_choice" in e.responses) || e.responses.user_single_choice,
                t = e.phase,
                a = e.responses,
                i = 1 / u(o).length;
            if ("initial" === t) return 0;
            if ("done" === t) return 1;
            var s = u(o).map(function(e) {
                return e.phase
            }).indexOf(t);
            return s * i + function(e, o) {
                for (var t = 0, a = 0; a < o.length; a++) o[a] in e && (t += 1);
                return t / o.length
            }(a, u(o)[s].form.sections.map(function(e) {
                return e.key
            })) * i
        }
    }
}
